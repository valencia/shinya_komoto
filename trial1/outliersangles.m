function [angles,dallpts] = outliersangles(mts, skipids)

angles = zeros(1,length(skipids));

allpts = [];

ids = 1:length(mts);
for i = 1:length(skipids)
    j = ids(:) == skipids(i);
    ids(j) = [];
end

for i = 1:length(ids)
    allpts = [allpts; mts(ids(i)).pts];
end

[~, dallpts] = line_fit_3D(allpts);

for i = 1:length(skipids)
    d = mts(skipids(i)).dirvect;
    angles(i) = acosd((dot(-dallpts, d))/(norm(-dallpts)*norm(d)));
end

mtlen = mts(1).mtlength;
r = centroid(allpts);
t = -mtlen:0.1:mtlen;
allfitx = r(1) + t*dallpts(1);
allfity = r(2) + t*dallpts(2);
allfitz = r(3) + t*dallpts(3);
plotmts(mts,'b',[17,18])
hold on
plot3(allfitx,allfity,allfitz,'r','LineWidth', 2)
hold off
legend('raw data', 'general dir')
view([97.4 11.6])
savepng('final1')
view(0,0)
savepng('final2')
view(0,90)
savepng('final3')
view(90,0)
savepng('final4')
close

M = [skipids;angles];
csvwrite('anglesdiffs.txt',M)



