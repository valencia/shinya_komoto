
function [mts, Xz, Xx, Xy] = evalmtsangles(mts)

% direction vectors
mts = getdirvect(mts);

% angle with normal to (x,y)-plane
mts = getzangles(mts,[0,0,1]);

% angle with x-axis
mts = getxangles(mts,[-1,0,0]);

% angle with y-axis
mts = getyangles(mts,[0,1,0]);

% get mts lengths
mts = getlenghts(mts);

% plot hist
[Xz, Xx, Xy] = angleshist(mts);

% visualise
plotmts(mts)
view([97.4 11.6])
savepng('all_mts')
close

end


%% direction vectors
function mtsdirvects = getdirvect(mts)

mtsdirvects = mts;
mtsdirvects(length(mts)).dirvect = [];
for i = 1:length(mts)
    
    mt = mts(i).pts;
    [~, mtsdirvects(i).dirvect] = line_fit_3D(mt);
    
end

end


%% angle with normal to (x,y)-plane
function mtsangles = getzangles(mts,v)
    mtsangles = mts;
    mtsangles(length(mts)).zangle = [];
    
    for i = 1:length(mts)
        d = mts(i).dirvect;
        mtsangles(i).zangle = acosd((dot(d, v))/(norm(d)*norm(v)));
    end

end

%% angle with x-axis
function mtsangles = getxangles(mts,v)
    mtsangles = mts;
    mtsangles(length(mts)).xangle = [];
    
    for i = 1:length(mts)
        d = mts(i).dirvect;
        mtsangles(i).xangle = acosd((dot(d, v))/(norm(d)*norm(v)));
    end

end

%% angle with x-axis
function mtsangles = getyangles(mts,v)
    mtsangles = mts;
    mtsangles(length(mts)).yangle = [];
    
    for i = 1:length(mts)
        d = mts(i).dirvect;
        mtsangles(i).yangle = acosd((dot(d, v))/(norm(d)*norm(v)));
    end

end

%% get mt length

function mtslengths = getlenghts(mts)
    mtslengths = mts;
    mtslengths(length(mts)).mtlength = []; 
    
    for i = 1:length(mts)
        mt = mts(i).pts;
        l = 0;
        for j = 1:length(mt)-1
            l = l+norm(mt(j+1,:)-mt(j,:));
        end
        mtslengths(i).mtlength = l; 
    end
    
end


%% plot hist
function [Xz,Xx,Xy] = angleshist(mts)

Az = zeros(length(mts),1);
Ax = zeros(length(mts),1);
Ay = zeros(length(mts),1);

for i = 1:length(mts)
    Az(i) = mts(i).zangle;
    Ax(i) = mts(i).xangle;
    Ay(i) = mts(i).yangle;
end

%A = acosd(A);

figure;
Xz = histogram(Az,21);
xlabel('angle from [0,0,1] (degrees)');
ylabel('number of mts');
savepng('histXz')
close

figure;
Xx = histogram(Ax,21);
xlabel('angle from [1,0,0] (degrees)');
ylabel('number of mts');
savepng('histXx')
close

figure;
Xy = histogram(Ay,21);
xlabel('angle from [0,1,0] (degrees)');
ylabel('number of mts');
savepng('histXy')
close
end




