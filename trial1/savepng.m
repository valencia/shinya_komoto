function savepng(filename, fig, dpi)

% function savepng(filename, fig, dpi)
%
% export figure 'fig' to file as a PNG image
% By default, dpi=150 and fig='current figure'
%
% F. Nedelec, April 2015

if nargin < 3
    %resolution (dpi) of final graphic
    dpi = 150;
end

if nargin < 2
    fig = gcf;
end

if nargin < 1
    filename = 'figure.png';
end

pos_pixels = getpixelposition(fig);
scr = get(0, 'ScreenPixelsPerInch');
pos_inches = pos_pixels(3:4)/scr;


set(fig, 'PaperUnits','inches','PaperSize',pos_inches,'PaperPosition',[0 0 pos_inches]);

if dpi == 150
    saveas(fig, filename, 'png');
else
    print(fig, filename, '-dpng', ['-r',num2str(dpi)], '-opengl')
end

end
