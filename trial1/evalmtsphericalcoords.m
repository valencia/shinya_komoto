
function [mts, X] = evalmtsphericalcoords(mts)

% direction vectors
mts = getdirvect(mts);

% angle with normal to plane
mts = getangles(mts,[0,0,1]);
mts = getangles(mts,[0,0,1]);
mts = getangles(mts,[0,0,1]);

% plot hist
X = angleshist(mts);

end


%% direction vectors
function mtsdirvects = getdirvect(mts)

mtsdirvects = mts;
mtsdirvects(length(mts)).dirvect = [];
for i = 1:length(mts)
    
    mt = mts(i).pts;
    [~, mtsdirvects(i).dirvect] = line_fit_3D(mt);
    
end

end

%% azimuth
function mtsangles = getangles(mts, vect)
    mtsangles = mts;
    mtsangles(length(mts)).angle = [];
    
    for i = 1:length(mts)
        d = mts(i).dirvect;
        mtsangles(i).angle = (dot(d, vect))/(norm(d)*norm(vect));
        
    end

end


%% plot hist
function histobj = angleshist(mts)

A = zeros(length(mts),1);

for i = 1:length(mts)
    A(i) = mts(i).angle;
end

A = acosd(A);

histobj = histogram(A,21);
xlabel('angle from [0,0,1] (degrees)')
ylabel('number of mts')
end

