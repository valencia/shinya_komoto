function hfig = plotmts(block, color, list, list2, hfig)
%
% Plots fibers of |block|
%
% Input:
% |block| obtained from load_fibers.m
% |color| = see MATLAB Documentation for possible colors for plot3 (do not choose black)
% |hfig| optional previous figure to plot on
% |list| optional list of mt that should be plotted in a different color. Use mt id.
% |list2| optional list of mt that should be plotted (i.e. if only want to plot certain fibers). Use mt id.
%
% Output:
% |hfig| is a matlab.ui/Figure variable
%
% Example:
% Plot block1, block2_t and block3_t in same figure
% h1 = sake_plot_fibers(block1,'b');
% h2t = sake_plot_fibers(block2_t,'g',h1);
% h1h2th3t = plot_fibers(block3_t,'r',h1h2t);
%
% Copyright Karin Sasaki and F. Nedelec, EMBL Heidelberg, 2015
% Created March 2015
%

% if no color specified
if nargin<2
    color = 'b';
end

% if not outstanding fibers
if nargin<3
    list = [];
end

% if want to plot all fibers in block
if nargin<4
    list2 = 1:length(block);
end

% If no |hfig| is specified, create new figure
if nargin < 5
    hfig = figure;
    hold on;
else
    figure(hfig);
end


% Plot
for i = list2
    %i
    mt_pts = block(i).pts;
    
    if isempty(mt_pts)
        continue
    end
    
    mt_id = block(i).id;
    
    if ~isempty(list)
        if sum(list==mt_id)==1
            plot3(mt_pts(:,1), mt_pts(:,2), mt_pts(:,3), 'k-*');
            %plot3(X,Y,Z,'k');
        else
            plot3(mt_pts(:,1), mt_pts(:,2), mt_pts(:,3), 'Color', color);
            %plot3(X,Y,Z,'g');
        end
    else
        plot3(mt_pts(:,1), mt_pts(:,2), mt_pts(:,3), 'Color', color);
        %plot3(X,Y,Z,'g');
    end
end


% Specify angle of view
az = 126;
el = 38;
view(az, el);

% Include axis label and grid
xlabel('x')
ylabel('y')
zlabel('z')
grid on
axis equal
%axis image

% Side view like in Light Microscopy image
 view(53,62);

return;



