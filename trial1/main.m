
function main()

mts = importfile('MicrotubuleCoordinates.txt', 2, 550);

plotmts(mts)
view(0,0)
savepng('allmts1')
view(0,90)
savepng('allmts2')
view(90,0)
savepng('allmts3')

[mts, ~,~,~] = evalmtsangles(mts);

skipids = [17,18];

[~,d] = outliersangles(mts, skipids);

visualisemts(mts,d);


