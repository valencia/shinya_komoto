function [] = visualisemts(mts,dallfit)

for i = 1:length(mts)
    
    mt = mts(i).pts;
    mtlen = mts(i).mtlength;
        
    pt = mt(1,:);
    
    t = 0:0.1:mtlen;
    
    d = mts(i).dirvect;
    
    vz = [0,0,1];
    vx = [1,0,0];
    vy = [0,1,0];
    
    xfitmt = pt(1) + t*d(1);
    yfitmt = pt(2) + t*d(2);
    zfitmt = pt(3) + t*d(3);
    
    xfitvz = pt(1) + t*vz(1);
    yfitvz = pt(2) + t*vz(2);
    zfitvz = pt(3) + t*vz(3);
    
    xfitvx = pt(1) + t*vx(1);
    yfitvx = pt(2) + t*vx(2);
    zfitvx = pt(3) + t*vx(3);
    
    xfitvy = pt(1) + t*vy(1);
    yfitvy = pt(2) + t*vy(2);
    zfitvy = pt(3) + t*vy(3);
    
    xf = pt(1) + t*dallfit(1);
    yf = pt(2) + t*dallfit(2);
    zf = pt(3) + t*dallfit(3);
    
    h=figure;
    plotmts(mts,'k',[i],[i],h)
    hold on
    plot3(xfitmt,yfitmt,zfitmt,'k')
    plot3(xfitvz,yfitvz,zfitvz, 'r')
    plot3(xfitvx,yfitvx,zfitvx, 'b')
    plot3(xfitvy,yfitvy,zfitvy, 'g')
    plot3(xf,yf,zf,'k--')
    hold off
    str = sprintf('mt %i', i);
    title(str)
    view([97.4 11.6])
    legend('raw data', 'linefit','x','y','z','general dir')
    
    str1 = sprintf('mt_%i', i);
    savepng(str1)
    close
end
